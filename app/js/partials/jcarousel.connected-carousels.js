// PHOTO ITEM ############
//-- START CAROUSEL BIG PHOTO --//
var itemphotocarousel = $('.item-photo-carousel').on('jcarousel:create jcarousel:reload', function() {
    var element = $(this),
        width = element.innerWidth();
    $('.item-photo-control').height(element.innerHeight() + 4 - ((element.innerHeight() + 4) % 3));
    $('.item-photo-control').height('125' + 'px');
    element.jcarousel('items').css('width', width + 'px');
}).jcarousel({
    animation: 0,
    wrap: 'both'
});
$('.item-photo .prev').jcarouselControl({
    target: '-=1',
    carousel: itemphotocarousel
});
$('.item-photo .next').jcarouselControl({
    target: '+=1',
    carousel: itemphotocarousel
});
//-- START CAROUSEL CONTROL PHOTOS --//
var itemphotocontrolcarousel = $('.item-photo-control-carousel').on('jcarousel:create jcarousel:reload', function() {
    var element = $(this),
        height = Math.round(element.innerHeight());
    element.jcarousel('items').css('height', height - 60 + 'px');
}).jcarousel({
    vertical: true,
    wrap: 'both'
});
$('.item-photo-control .prev').jcarouselControl({
    target: '-=1',
    carousel: itemphotocontrolcarousel
});
$('.item-photo-control .next').jcarouselControl({
    target: '+=1',
    carousel: itemphotocontrolcarousel
});
//-- CONECTOR CAROUSEL BIG PHOTO WITH CONTROL PHOTOS --//
var connector = function(itemNavigation, carouselStage) {
    return carouselStage.jcarousel('items').eq(itemNavigation.index());
};
itemphotocontrolcarousel.jcarousel('items').each(function() {
    var item = $(this);
    var target = connector(item, itemphotocarousel);
    item.on('jcarouselcontrol:active', function() {
        itemphotocontrolcarousel.jcarousel('scrollIntoView', this);
        item.addClass('active');
    }).on('jcarouselcontrol:inactive', function() {
        item.removeClass('active');
    }).jcarouselControl({
        target: target,
        carousel: itemphotocarousel
    });
});
// PHOTO ITEM ############