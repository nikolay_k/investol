/**
 * Author: Nikolay Kovalenko
 * Start Date: 18.05
 * End Date: now
 * Author Email: kovalenko_mykola@ukr.net
 */

$(document).ready(
    function() {
        var $mainSlider = $('#main-banners');
        if ($mainSlider.length) {
            $mainSlider.slick({
                dots: true,
                lazyLoad: 'ondemand',
                infinite: true,
                autoplay: false,
                autoplaySpeed: 5000,
                arrows: false,
                responsive: [{
                    breakpoint: 767,
                    settings: {
                        arrows: false
                    }
                }]
            });
        }

        var clients = $('.clients-slider');
        if (clients.length) {
            clients.slick({
                dots: true,
                lazyLoad: 'ondemand',
                autoplay: true,
                infinite: true,
                autoplaySpeed: 5000,
                slidesToShow: 5,
                slidesToScroll: 1,
                arrows: false,
                responsive: [{
                    breakpoint: 1300,
                    settings: {
                        slidesToShow: 5,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: true
                    }
                },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }

                ]
            });
        }


        $('.works').slick({
            infinite: true,
            dots: true,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: false,
            responsive: [{
                breakpoint: 1300,
                settings: {
                    slidesToShow: 6,
                    slidesToScroll: 6,
                    infinite: true,
                    dots: true
                }
            },
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 1100,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 980,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }

            ]
        });


        /**
         * Search Animation
         */


        $('.popup-with-zoom-anim').magnificPopup({
            type: 'inline',

            fixedContentPos: false,
            fixedBgPos: true,

            overflowY: 'auto',

            closeBtnInside: true,
            preloader: false,

            midClick: true,
            removalDelay: 300,
            mainClass: 'my-mfp-zoom-in'
        });



        $(document).on('click', '.popup-modal-dismiss', function(e) {
            e.preventDefault();
            $.magnificPopup.close();
        });

    }
);


/**
 * Bootstrap btn Animate on active
 * @param {Object} btn
 */

function checkState(btn) {
    var btnClasses = btn.classList;
    if (btnClasses.contains("collapsed")) {
        btn.classList.add("open");
    } else {
        btn.classList.remove("open");
    }

}

document.querySelector('.navbar button').onclick = function() {
    checkState(this)
};


/**
 * @param {Object} parent
 * @param {Object} elem
 */

function selectLogic(parent, elem) {
    var result = $('#' + elem).empty();
    var selected = parent.find('li.ui-selected').text();
    $('#' + elem).text(selected);
    parent.parent('.select-block').removeClass('active');
    $(".ui-selected").each(function() {
        var imgSrc = $(this).attr('data-image');
        $('#item-pic').attr('src', imgSrc);
    });
}


/**
 * Selecteble Block INIT
 */

$(function() {
    var selectCount = $(".select-block").length;
    for (var k = 0; k < selectCount; k++) {
        $("#select-" + (k + 1)).selectable({
            stop: function() {
                selectLogic($(this), $(this).attr('data-target'))
            }
        });
    }
});


/**
 * Select Color/Material/Structure Logic
 */

$(function() {
    $('.select').click(function() {
        $('.select-block').removeClass('active');
        var targetBlock = $(this).attr('id');
        $('.' + targetBlock + '-block').toggleClass('active');
    });
});


/**
 * Quantyti block
 */

$(function() {
    $('.quantity').each(function() {
        var spinner = jQuery(this),
            input = spinner.find('input[type="number"]'),
            btnUp = spinner.find('.quantity-up'),
            btnDown = spinner.find('.quantity-down'),
            min = input.attr('min'),
            max = input.attr('max');

        btnUp.click(function() {
            var oldValue = parseFloat(input.val());
            if (oldValue >= max) {
                var newVal = oldValue;
            } else {
                var newVal = oldValue + 1;
            }
            spinner.find("input").val(newVal);
            spinner.find("input").trigger("change");
        });

        btnDown.click(function() {
            var oldValue = parseFloat(input.val());
            if (oldValue <= min) {
                var newVal = oldValue;
            } else {
                var newVal = oldValue - 1;
            }
            spinner.find("input").val(newVal);
            spinner.find("input").trigger("change");
        });

    });
});


/**
 * Show Block
 */

$('.add-info').click(function() {
    $('.info').toggleClass('active');
});

$('.close').click(function() {
    $('.info').removeClass('active');
});


/**
 * Change ordrering Elements into mobile
 */

function orderBlock(elemClass, toSide) {

    var selTarget = $('#select .widget');
    var selBlock = $('#select-block-wrap .' + elemClass);
    var arrSelect = [];

    if (toSide == true) {
        selBlock = $('#select .' + elemClass);
    }

    for (var i = 0; i < selTarget.length; i++) {
        if (selTarget[i].dataset.select != undefined) {
            arrSelect.push(selTarget[i].dataset.select);
        }
    }

    for (select in arrSelect) {
        if (elemClass == arrSelect[select]) {
            if (toSide == true) {
                selBlock.appendTo($('#select-block-wrap'));
            } else {
                selBlock.appendTo($('#select .widget[data-select=' + arrSelect[select] + ']'));
            }
        }
    }
}

enquire.register("screen and (max-width:991px)", {
    deferSetup: true,
    setup: function() {},
    match: function() {
        console.log('991');
        orderBlock('select-color-block');
        orderBlock('select-material-block');
        orderBlock('select-strucutre-block');
    },
    unmatch: function() {
        orderBlock('select-color-block', true);
        orderBlock('select-material-block', true);
        orderBlock('select-strucutre-block', true);
    }
});

/**
 * hOVER dROPdOWN
 */


$(document).ready(function () {

    if ($(window).width() > 768) {
        var that;
        $(".dropdown").hover(
            function (e) {
                that = this;
                $(this).children(".dropdown-menu").stop(true, false).fadeToggle('400');
                $(this).toggleClass('open');
                e.preventDefault();
            }
        );
    }
});



/**
 * Sub DropDown Menu Logic
 */


(function() {
    var subMenu = $('.sub-dropdown li');
    var subMenuSub = $('.sub-dropdown-sub');
    $('.sub-dropdown li.sub a').hover(function() {
        subMenuSub
            .addClass('hide')
            .hide()
            .find('li')
            .addClass('hide')
            .hide();

        var dataSubMenu = $(this).data('menu');

        subMenuSub
            .removeClass('hide')
            .show()
            .find('li[data-menu-target="' + dataSubMenu + '"]')
            .removeClass('hide')
            .show();
    }, function(e) {
        var classs = $($(e.relatedTarget)[0]);
        if (!classs.is("ul.sub-dropdown.col-md-6")) {
            subMenuSub
                .addClass('hide')
                .hide()
                .find('li')
                .addClass('hide')
                .hide();
        } else {
            console.log(false);
        }
    });

    $('ul.sub-dropdown-sub.col-md-6, .dropdown-menu.mega-dropdown-menu').on('mouseleave', function(e) {
        subMenuSub
            .addClass('hide')
            .hide()
            .find('li')
            .addClass('hide')
            .hide();
    });


    $('#back-call,#consult-form').on('submit', function(e) {
        e.preventDefault();
        //$.magnificPopup.close();
        $.magnificPopup.open({ items: { src: '#thank', type: 'inline' } });
    })
})();


$('#accordion').on('show.bs.collapse', function() {
    if ($('#text').text() == "Скрыть") {
        $('#text').text('Показать');
    } else {
        $('#text').text('Скрыть')
    }
});

$('#accordion').on('hide.bs.collapse', function() {
    if ($('#text').text() == "Показать") {
        $('#text').text('Скрыть');
    } else {
        $('#text').text('Скрыть')
    }
});